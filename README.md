# Bahn payed

Ich wollte einfach mal wissen wie viel Geld ich schon an die Bahn bezahlt habe. Somit war die Idee für dieses Tool gebohren.

## Nutzung
Die Punkteübersicht auf den gewünschten Zeitraum einstellen und dann die Druckversion in einer Datei speichern. Wenn sie nicht index.html heißt, muss der Name über -i angegeben werden
```shell script
go run main.go [--sum] [--month] [--every] [--json] [--table] [--input=string]

Description:
    Bahn Bonus Druckübersicht Terminal Interpreter

Options:
    -s, --sum     Alle Preise aufsummieren
    -m, --month   Summe pro Monat (Kauf)
    -e, --every   Gebe jede Zeile in der console aus
        --json    exportier Daten als JSON
    -t, --table   Daten als Tabelle darstellen
    -i, --input   spezifiziere die html Datei
```

Wenn eine Binary verwendet wird, dann kann `go run main.go` durch den Namen der Datei ersetzt werden.

## Roadmap
Da man nur maximal 39 Monate auf einmal abrufen kann, möchte ich gerne einen Zwischenspeicher einbauen, der auch Duplikate erkennt. 
package main

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/gosuri/uitable"
	"github.com/teris-io/cli"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

var content = []Row{}

type Row struct {
	ID string
	Date string
	Category string
	Amount float64
	StatusPoints int
	PaybackPoints int
	Currency string
	Details string
}

func main() {

	app := cli.New("Bahn Bonus Druckübersicht Terminal Interpreter").
		WithOption(cli.NewOption("sum", "Alle Preise aufsummieren").WithChar('s').WithType(cli.TypeBool)).
		WithOption(cli.NewOption("month", "Summe pro Monat (Kauf)").WithChar('m').WithType(cli.TypeBool)).
		WithOption(cli.NewOption("every", "Gebe jede Zeile in der console aus").WithChar('e').WithType(cli.TypeBool)).
		WithOption(cli.NewOption("json", "exportier Daten als JSON").WithType(cli.TypeBool)).
		WithOption(cli.NewOption("table", "Daten als Tabelle darstellen").WithChar('t').WithType(cli.TypeBool)).
		WithOption(cli.NewOption("input", "spezifiziere die html Datei").WithChar('i').WithType(cli.TypeString)).
		WithAction(func(args []string, options map[string]string) int {
			fmt.Println("Bahn Buchungsübersicht")
			fmt.Println("----------------------")
			file := "./index.html"

			if options["input"] != "" {
				file = options["input"]
			}

			f, err := os.Open(file)
			if err != nil {
				panic(err)
				os.Exit(2)
			}
			defer f.Close()
			processTable(f)

			// Print Table
			if test(options, "table") {
				fmt.Println()
				fmt.Println()
				fmt.Println("Übersicht aller Buchungen")
				fmt.Println()
				printEntriesShort()
			}

			// Print Table
			if test(options, "every") {
				fmt.Println()
				fmt.Println()
				fmt.Println("Übersicht aller Buchungen")
				fmt.Println()
				printEntries()
			}

			// Print Month
			if test(options, "month") {
				fmt.Println()
				fmt.Println()
				fmt.Println("Übersicht der monatlichen Ausgaben")
				fmt.Println()
				printMonth()
			}

			// Print Sum
			if test(options, "sum") {
				fmt.Println()
				fmt.Println()
				fmt.Println("Summe aller Buchungen")
				printSum()
			}

			// Export
			if options["json"] != "" {
				b, _ := json.MarshalIndent(content, "", "  ")
				ioutil.WriteFile("output.json", b, 0644)

			}

			if len(options) == 0 {
				fmt.Println("Bitte gib an, welche Information du möchtest. Alle Optionen kannst du dir mit --help anzeigen.")
			}
			return 0
	})

	os.Exit(app.Run(os.Args, os.Stdout))
}

func processTable(file *os.File) {
	doc, err := goquery.NewDocumentFromReader(file)
	if err != nil {

	}

	doc.Find("tbody").Children().Each(func(i int, s *goquery.Selection) {
		r := Row{}
		r.ID = s.Find(".col11").Text()
		if strings.TrimSpace(r.ID) == "" {return}
		r.Date = s.Find(".col3").Text()
		r.Category = s.Find(".col4").Text()
		a := s.Find(".col5").Text()
		a = strings.TrimSuffix(a, " EUR")
		r.Currency = "EUR"
		a = strings.ReplaceAll(a, ",", ".")
		r.Amount, _ = strconv.ParseFloat(a, 64)
		status := s.Find(".col6").Text()
		r.StatusPoints, _ = strconv.Atoi(status)
		payback := s.Find(".col7").Text()
		r.PaybackPoints, _ = strconv.Atoi(payback)
		details := ""
		s.Find(".col8").Contents().Each(func(i int, s *goquery.Selection) {
			if s.Is("br"){
				details += "; "
			}else {
				details += strings.TrimSpace(s.Text())
			}
		})
		r.Details = strings.TrimSuffix(details, "; ")

		content = append(content, r)
	})
}

func printEntries() {
	table := uitable.New()
	table.MaxColWidth = 80

	table.AddRow("ID", "Datum", "Kategorie", "Betrag", "SP", "BP", "Details")
	table.AddRow("--", "-----", "---------", "------", "--", "--", "-------")
	for _, r := range content {
		table.AddRow(r.ID, r.Date, r.Category, fmt.Sprintf("%.2f €",  r.Amount), r.StatusPoints, r.PaybackPoints, r.Details)
	}

	fmt.Println(table)
}

func printEntriesShort() {
	table := uitable.New()
	table.MaxColWidth = 50

	table.AddRow( "Datum", "Kategorie", "Betrag")
	table.AddRow("-----", "---------", "------")
	sumB := 0.0
	for _, r := range content {
		table.AddRow( r.Date, r.Category, fmt.Sprintf("%.2f €",  r.Amount))
		sumB += r.Amount
	}
	table.AddRow("","", fmt.Sprintf("%.2f €", sumB))

	fmt.Println(table)
}

func printMonth() {
	table := uitable.New()
	table.MaxColWidth = 50
	table.RightAlign(2)
	table.RightAlign(1)

	table.AddRow( "Monat", "Betrag", "Bonuspunkte")
	table.AddRow("-----", "     ------", "-----------")
	cur := getMonth(content[0].Date)
	sumB := 0.0
	sumP := 0
	for _, r := range content {
		if cur == getMonth(r.Date) {
			sumB += r.Amount
			sumP += r.PaybackPoints
		}else {
			table.AddRow(cur, fmt.Sprintf("%.2f %s",  sumB, "€"), sumP)
			sumP = r.PaybackPoints
			sumB = r.Amount
			cur = getMonth(r.Date)
		}
	}

	fmt.Println(table)
}

func printSum() {
	sum := 0.0
	for _, r := range content {
		sum += r.Amount
	}
	fmt.Printf("  %.2f €\n", sum)
}

func test(options map[string]string, key string) bool {
	return options[key] == "true"
}

func getMonth(date string) string {
	return strings.SplitAfterN(strings.TrimSpace(date), ".", 2)[1]
}